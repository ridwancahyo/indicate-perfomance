<?php

use Illuminate\Database\Seeder;
use App\Position;
use App\User;
use App\Employee;
use App\Satuan;
use App\Indicator;
use App\Proxy;
use App\SubProxy;
use App\CalculateSubProxy;
use App\RumusSubProxy;
use App\ScoreIndicator;

class AllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create user
        User::create([
            'name' => 'User Testing',
            'email' => 'testing@gmail.com',
            'password' => bcrypt('password'),
        ]);

        //create position
        Position::create([
            'image' => 'images/position/1/1605259162ss.png',
            'name' => 'Sales Manager',
            'description' => 'It is a long established fact that a reader will be distracted by.',
            'user_id' => 1,
        ]);

        //create pegawai
        Employee::create([
            'kode' => '0123456',
            'nama' => 'Wanco',
            'jenis_kelamin' => 'Pria',
            'email' => 'wanco@gmail.com',
            'no_telephone' => '081910094095',
            'position_id' => 1,
            'user_id' => 1,
        ]);

        //create satuan
        Satuan::create([
            'name' => 'Rp',
            'status' => 1,
            'user_id' => 1,
        ]);

        //create Indikator
        Indicator::insert(
            [
                [
                    'name' => 'Sales',
                    'description' => 'asdkjasdkjaskdj',
                    'bobot' => 30,
                    'position_id' => 1,
                    'target' => 0,
                    'satuan_id' => 1,
                    'user_id' => 1,
                    'type' => 'positive',
                ],
                [
                    'name' => 'Biaya',
                    'description' => 'asdkjasdkjaskdj',
                    'bobot' => 30,
                    'position_id' => 1,
                    'target' => 0,
                    'satuan_id' => 1,
                    'user_id' => 1,
                    'type' => 'negative',
                ],
                [
                    'name' => 'Promosi',
                    'description' => 'asdkjasdkjaskdj',
                    'bobot' => 40,
                    'position_id' => 1,
                    'target' => 0,
                    'satuan_id' => 1,
                    'user_id' => 1,
                    'type' => 'positive',
                ],
            ]
        );

        //insert proxy
        Proxy::insert(
            [
                [
                    'name' => 'Sales Produk A',
                    'bobot' => 30,
                    'target' => 5000000,
                    'satuan_id' => 1,
                    'indicator_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Sales Produk B',
                    'bobot' => 30,
                    'target' => 4000000,
                    'satuan_id' => 1,
                    'indicator_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Sales Produk C',
                    'bobot' => 40,
                    'target' => 10000000,
                    'satuan_id' => 1,
                    'indicator_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Biaya Produk A',
                    'bobot' => 30,
                    'target' => 3000000,
                    'satuan_id' => 1,
                    'indicator_id' => 2,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Biaya Produk B',
                    'bobot' => 30,
                    'target' => 2000000,
                    'satuan_id' => 1,
                    'indicator_id' => 2,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Biaya Produk C',
                    'bobot' => 40,
                    'target' => 7000000,
                    'satuan_id' => 1,
                    'indicator_id' => 2,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Promosi Produk A',
                    'bobot' => 30,
                    'target' => 200,
                    'satuan_id' => 1,
                    'indicator_id' => 3,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Promosi Produk B',
                    'bobot' => 30,
                    'target' => 100,
                    'satuan_id' => 1,
                    'indicator_id' => 3,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Promosi Produk C',
                    'bobot' => 40,
                    'target' => 150,
                    'satuan_id' => 1,
                    'indicator_id' => 3,
                    'user_id' => 1,
                ],
            ]
        );

        //insert subproxy
        SubProxy::insert(
            [
                [
                    'name' => 'Jumlah Sales Produk A',
                    'code' => 'A',
                    'proxy_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk A',
                    'code' => 'B',
                    'proxy_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Sales Produk B',
                    'code' => 'C',
                    'proxy_id' => 2,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk B',
                    'code' => 'D',
                    'proxy_id' => 2,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Sales Produk C',
                    'code' => 'E',
                    'proxy_id' => 3,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk C',
                    'code' => 'F',
                    'proxy_id' => 3,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Sales Produk A',
                    'code' => 'G',
                    'proxy_id' => 4,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk A',
                    'code' => 'H',
                    'proxy_id' => 4,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Sales Produk B',
                    'code' => 'I',
                    'proxy_id' => 5,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk B',
                    'code' => 'J',
                    'proxy_id' => 5,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Sales Produk C',
                    'code' => 'K',
                    'proxy_id' => 6,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Harga Sales Produk C',
                    'code' => 'L',
                    'proxy_id' => 6,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Promosi Produk A',
                    'code' => 'M',
                    'proxy_id' => 7,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Promosi Produk B',
                    'code' => 'N',
                    'proxy_id' => 8,
                    'user_id' => 1,
                ],
                [
                    'name' => 'Jumlah Promosi Produk C',
                    'code' => 'O',
                    'proxy_id' => 9,
                    'user_id' => 1,
                ],
            ]
        );

    }
}
