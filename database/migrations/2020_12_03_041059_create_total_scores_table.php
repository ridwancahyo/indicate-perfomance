<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('total_score', 11, 2);
            $table->string('predicate', 10);
            $table->integer('employee_id');
            $table->integer('user_id');
            $table->tinyInteger('recap')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_scores');
    }
}
