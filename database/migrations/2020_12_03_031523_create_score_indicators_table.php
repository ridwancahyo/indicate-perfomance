<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_indicators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('score', 11, 2);
            $table->integer('indicator_id');
            $table->integer('employee_id');
            $table->integer('user_id');
            $table->tinyInteger('recap')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_indicators');
    }
}
