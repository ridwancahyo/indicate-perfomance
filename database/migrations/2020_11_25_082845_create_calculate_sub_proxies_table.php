<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculateSubProxiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculate_sub_proxies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('param_data');
            $table->string('rumus');
            $table->decimal('hasil', 11,2);
            $table->decimal('score_proxy', 11, 2);
            $table->integer('proxy_id');
            $table->integer('employee_id');
            $table->integer('user_id');
            $table->tinyInteger('recap')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculate_sub_proxies');
    }
}
