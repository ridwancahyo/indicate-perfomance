@extends('layouts.main_app')
@push('css')
@include('admin.subproxy.css')
@endpush
@push('script')
@include('admin.subproxy.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Sub Proxy</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Sub Proxy</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Sub Proxy</h3>
                    </div>
                    @php
                        $url = route('subproxy.store');
                        if (!empty($subproxy)) {
                            $url = route('subproxy.update',$subproxy->id);
                        }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($subproxy))
                            @method('PUT')
                        @endif
                        <div class="card-body">
                            {{--  <div class="form-group">
                                <label for="proxy_id">Nama Proxy</label>
                                <select name="proxy_id" id="proxy_id" required class="form-control">
                                    <option value="">-- Pilih Proxy --</option>
                                    @foreach ($proxies as $proxy)
                                        <option value="{{ $proxy->id }}"  @if ($proxy->id == @$subproxy->id) selected @endif >{{ $proxy->name }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                            <div class="form-group">
                                <label for="name">Nama Sub Proxy</label>
                                <input type="text" class="form-control" id="name" placeholder="Masukkan nama" name="name" required value="{{ @$subproxy->name }}">
                            </div>
                            <div class="form-group">
                                <label for="code">Kode Sub Proxy</label>
                                <input type="text" class="form-control" id="code" placeholder="Masukkan code" name="code" required value="{{ @$subproxy->code }}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection