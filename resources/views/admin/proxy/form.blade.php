@extends('layouts.main_app')
@push('css')
@include('admin.proxy.css')
@endpush
@push('script')
@include('admin.proxy.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Proxy</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Proxy</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Proxy</h3>
                    </div>
                    @php
                        $url = route('proxy.store');
                        if (!empty($proxy)) {
                            $url = route('proxy.update',$proxy->id);
                        }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($proxy))
                            @method('PUT')
                        @endif
                        <div class="card-body">
                            {{--  <div class="form-group">
                                <label for="name">Indikator</label>
                                <select name="indicator_id" id="indicator_id" class="form-control" required>
                                    <option value="">-- Pilih Indikator --</option>
                                    @foreach ($indicators as $indicator)
                                        <option value="{{ $indicator->id }}" @if (@$indicator->id == @$proxy->indicator_id) selected @endif>{{ $indicator->name }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                            <div class="form-group">
                                <label for="name">Nama Proxy</label>
                                <input type="text" class="form-control" id="name" placeholder="Masukkan nama" name="name" required value="{{ @$proxy->name }}">
                            </div>
                            {{--  <div class="form-group">
                                <label for="bobot">Bobot Proxy</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control col-md-4" name="bobot" required value="{{ @$proxy->bobot }}">
                                    <div class="input-group-append">
                                      <span class="input-group-text"><i class="fa fa-percent"></i></span>
                                    </div>
                                </div>
                            </div>  --}}
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection