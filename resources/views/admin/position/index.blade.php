@extends('layouts.main_app')
@push('css')
@include('admin.position.css')
@endpush
@push('script')
@include('admin.position.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Jabatan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Jabatan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="text-right">
                            <a href="{{ route('position.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jabatan</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Jabatan</th>
                                    {{--  <th>Indikator</th>  --}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($positions as $position)
                                    <tr>
                                        <td>{{ $position->name }}</td>
                                        {{--  <td>
                                            <ul>
                                                @foreach ($position->indicators as $indicator)
                                                    <li>{{ $indicator->name }} ({{ $indicator->bobot }} %)</li>
                                                @endforeach
                                            </ul>
                                        </td>  --}}
                                        <td> 
                                            <table>
                                                <tr>
                                                    <td><a href="{{ route('position.edit',$position->id) }}" class="btn btn-warning btn-sm mr-1"><i class="fa fa-edit"></i></a></td>
                                                    <td><form action="{{ route('position.destroy', $position->id) }}" method="POST">@csrf @method('DELETE')  <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('apakah anda yakin ?')"><i class="fa fa-trash"></i></button> </form></td>
                                                    <td><a href="{{ route('position.indexparamsdata', $position->id) }}" class="btn btn-primary btn-sm">Param Data</a></td> 
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">Data no found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection