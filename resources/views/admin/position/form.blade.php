@extends('layouts.main_app')
@push('css')
@include('admin.position.css')
@endpush
@push('script')
@include('admin.position.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Jabatan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Jabatan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Jabatan</h3>
                    </div>
                    @php
                        $url = route('position.store');
                        if (!empty($position)) {
                            $url = route('position.update',$position->id);
                        }
                    @endphp
                    <form action="{{ $url }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if (!empty($position))
                            @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Nama Jabatan</label>
                                <input type="text" class="form-control" id="name" placeholder="Masukkan name" name="name" required value="{{ @$position->name }}">
                            </div>
                            <div class="form-group">
                                <label for="description">Deskripsi</label>
                                <textarea name="description" placeholder="Masukkan deskripsi" id="description" cols="30" rows="5" class="form-control" required>{{ @$position->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Photo</label>
                                <input type="file" name="image" id="image" accept="images/*" class="form-control" >
                                <small>format yang didukung: jpeg, bmp, png</small>
                                @if (!empty(@$position->image))
                                    <br><img src="{{ asset($position->image) }}" alt="image_position" width="150px" height="100px">
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection