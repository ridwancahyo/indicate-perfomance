@extends('layouts.main_app')
@push('css')
@include('admin.indicator.css')
@endpush
@push('script')
@include('admin.indicator.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Indicator</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Indicator</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Indicator</h3>
                    </div>
                    @php
                    $url = route('indicator.store');
                    if (!empty($indicator)) {
                        $url = route('indicator.update',$indicator->id);
                    }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($indicator))
                        @method('PUT')
                        @endif
                        <div class="card-body">
                            {{--  <div class="form-group">
                                <label for="position_id">Jabatan</label>
                                <select name="position_id" id="position_id" class="form-control" required>
                                    <option value="">-- Pilih Jabatan --</option>
                                    @foreach ($positions as $position)
                                        <option value="{{ $position->id }}" @if ($position->id == @$indicator->position_id) selected @endif >{{ $position->name }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                            <div class="form-group">
                                <label for="name">Nama Indicator</label>
                                <input type="text" class="form-control" id="name" placeholder="Masukkan nama" name="name" required value="{{ @$indicator->name }}">
                            </div>
                            {{--  <div class="form-group">
                                <label for="bobot">Bobot Indicator</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control col-md-4" name="bobot" required value="{{ @$indicator->bobot }}">
                                    <div class="input-group-append">
                                      <span class="input-group-text"><i class="fa fa-percent"></i></span>
                                    </div>
                                </div>
                            </div>  --}}
                            <div class="form-group">
                                <label for="description">Deskripsi</label>
                                <textarea name="description" id="description" cols="30" rows="5" placeholder="Masukkan deskripsi" class="form-control" required>{{ @$indicator->description }}</textarea>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection