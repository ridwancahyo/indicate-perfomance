@extends('layouts.main_app')
@push('css')
@include('admin.indicator.css')
@endpush
@push('script')
@include('admin.indicator.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Proxy</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item">Indikator</li>
                    <li class="breadcrumb-item active">Proxy</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <h4>{{ @$indicator->position->name }} / {{ @$indicator->name }}</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Proxy</th>
                                    <th>Bobot</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($proxies as $proxy)
                                <tr>
                                    <td>{{ $proxy->name }}</td>
                                    <td>{{ $proxy->bobot }} %</td>
                                    <td>
                                        <form action="{{ route('indicator.deleteProxy', $proxy->id) }}" method="POST">
                                        {{ csrf_field() }} @method('DELETE')
                                        <button type="submit" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">Data not found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <br><br>
                        <h4>Tabel List Proxy</h4>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Proxy</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($otherProxies as $otherproxy)
                                <tr>
                                    <td>{{ $otherproxy->name }}</td>
                                    <td>
                                        <button id="add-proxy-btn" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-proxy" data-id="{{ $otherproxy->id }}">
                                            <i class="fa fa-plus"></i>
                                          </button>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3">Data not found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@include('admin.indicator.proxy.modal-add-proxy')
@endsection