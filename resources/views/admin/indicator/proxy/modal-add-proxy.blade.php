<div class="modal fade" id="modal-add-proxy">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('indicator.addProxy.post') }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Proxy</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="proxy_id" id="proxy_id_add">
                        <input type="hidden" name="indicator_id" id="indicator_id_add" value="{{ $indicator->id }}">
                        <label for="bobot">Bobot Proxy</label>
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" min="0" max="100" name="bobot" required>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-percent"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>