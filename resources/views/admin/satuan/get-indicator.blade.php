@extends('layouts.main_app')
@push('css')
@include('admin.position.css')
@endpush
@push('script')
@include('admin.position.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Indikator</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item">Jabatan</li>
                    <li class="breadcrumb-item active">Indikator</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <h4>{{ $position->name }}</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Indikator</th>
                                    <th>Bobot</th>
                                    <th>Deskripsi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($indicators as $indicator)
                                <tr>
                                    <td>{{ $indicator->name }}</td>
                                    <td>{{ $indicator->bobot }} %</td>
                                    <td>{{ $indicator->description }}</td>
                                    <td>
                                        <form action="{{ route('position.deleteIndicator', $indicator->id) }}" method="POST">
                                        {{ csrf_field() }} @method('DELETE')
                                        <button type="submit" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">Data not found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <br><br>
                        <h4>Tabel List Indikator</h4>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Indikator</th>
                                    <th>Deskripsi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($otherIndicators as $otherIndicator)
                                <tr>
                                    <td>{{ $otherIndicator->name }}</td>
                                    <td>{{ $otherIndicator->description }}</td>
                                    <td>
                                        <button id="add-indicator-btn" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-indicator" data-id="{{ $otherIndicator->id }}">
                                            <i class="fa fa-plus"></i>
                                          </button>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3">Data not found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->

{{--  include Modal  --}}
@include('admin.position.modal-add-indicator')
@endsection