@extends('layouts.main_app')
@push('css')
@include('admin.satuan.css')
@endpush
@push('script')
@include('admin.satuan.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Satuan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Satuan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Satuan</h3>
                    </div>
                    @php
                        $url = route('satuan.store');
                        if (!empty($satuan)) {
                            $url = route('satuan.update',$satuan->id);
                        }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($satuan))
                            @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Nama Satuan</label>
                                <input type="text" class="form-control" id="name" placeholder="Masukkan name" name="name" required value="{{ @$satuan->name }}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection