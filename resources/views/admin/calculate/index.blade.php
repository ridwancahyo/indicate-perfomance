@extends('layouts.main_app')
@push('css')
@include('admin.calculate.css')
@endpush
@push('script')
@include('admin.calculate.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $employee->nama }}</h1>

            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                    <li class="breadcrumb-item active">Calculation</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <p class="col-md-12">{{ $employee->position->name }}</p>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="text-left">
                                <h4>Penilaian</h4>
                            </div>
                            <div class="mx-auto">
                                <h4 class="text-danger">Total Score : {{ (@$totalScore->total_score) ?? 0 }} ({{ (@$totalScore->predicate) ?? '-' }})</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Indikator</th>
                                    <th>Bobot</th>
                                    <th>Type</th>
                                    <th>Score</th>
                                    <th>Proxy</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($indicators as $indicator)
                                <tr>
                                    <td>{{ $indicator->name }}</td>
                                    <td>{{ $indicator->bobot }} %</td>
                                    {{-- <td>{{ number_format($indicator->target) }} {{ $indicator->satuan->name }}</td> --}}
                                    <td>{{ $indicator->type }}</td>
                                    <td>{{ number_format((@\DB::table('score_indicators')->where('employee_id', Request::segment(3))->where('user_id', user_login()->id)->where('recap',0)->where('indicator_id', $indicator->id)->first()->score) ?? 0,2) }} %</td>
                                    <td>
                                        <table class="table">
                                            <tr>
                                                <td>Nama Proxy</td>
                                                <td>Bobot</td>
                                                <td>Target</td>
                                                <td>Score</td>
                                                <td>Sub Proxy</td>
                                            </tr>
                                            @foreach ($indicator->proxies as $proxy)
                                            @foreach ($result as $rs)
                                                @php
                                                    if ($proxy->id == $rs->proxy_id) {
                                                        @$param_data[$proxy->id] = json_decode($rs->param_data, true);
                                                        $hasilnya[$proxy->id] = $rs->hasil;
                                                    }
                                                @endphp
                                            @endforeach
                                            <tr>
                                                <td>{{ $proxy->name }}</td>
                                                <td>{{ $proxy->bobot }} %</td>
                                                <td>{{ number_format($proxy->target) }} {{ $proxy->satuan->name }}</td>
                                                <td>{{ number_format((@\DB::table('calculate_sub_proxies')->where('recap',0)->where('user_id', user_login()->id)->where('employee_id', Request::segment(3))->where('proxy_id', $proxy->id)->first()->score_proxy) ?? 0,2) }} %</td>
                                                <td>
                                                    <form action="{{ route('calculate.subproxy') }}" method="post" id="form_{{ $proxy->id }}">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="indicator_id" id="indicator_id" value="{{ $indicator->id }}">
                                                        <input type="hidden" name="employee_id" value="{{ Request::segment(3) }}">
                                                        <input type="hidden" name="proxy_id" id="proxy_id" value="{{ $proxy->id }}">
                                                    <p><strong>Rumus:
                                                    @foreach ($rumus as $rm)
                                                    @if ($rm->proxy_id == $proxy->id)
                                                    {{ $rm->rumus }}
                                                    <input type="hidden" name="rumus" id="rumus" value="{{ $rm->rumus }}">
                                                    @endif
                                                    @endforeach
                                                    </strong></p>
                                                    <p><strong>Nilai: {{ number_format((@$hasilnya[$proxy->id]) ? $hasilnya[$proxy->id] : 0, 2) }}</strong></p>
                                                    <table class="table">
                                                        <tr>
                                                            <td>Nama Sub Proxy</td>
                                                            <td>Kode</td>
                                                            <td>Score</td>
                                                        </tr>
                                                        @foreach ($proxy->subproxies as $subproxy)

                                                        <tr>
                                                            <td>{{ $subproxy->name }}</td>
                                                            <td>{{ $subproxy->code }}</td>
                                                            <td>
                                                                    <input autocomplete="off" type="number" name="nilai[{{ $subproxy->code }}]" id="nilai" style="width: 70px" min="0" value="{{ @$param_data[$proxy->id][$subproxy->code] }}" required {{ ($disabledCalculate) ? 'disabled' : ' ' }}>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    @if ($proxy->subproxies->count() > 0 && !$disabledCalculate)
                                                    <button class="btn btn-primary col-md-12" type="submit">Hitung</button>
                                                    @endif
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br><br>
                        @if (!$disabledCalculate)
                        <form action="{{ route('recap.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                            <button type="submit" class="btn btn-success col-md-12">Rekap Penilaian</button>
                        </form>
                        @endif
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
