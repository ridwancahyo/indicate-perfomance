@extends('layouts.main_app')
@push('css')
@include('admin.employee.css')
@endpush
@push('script')
@include('admin.employee.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pegawai</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Pegawai</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Pegawai</h3>
                    </div>
                    @php
                        $url = route('employee.store');
                        if (!empty($employee)) {
                            $url = route('employee.update',$employee->id);
                        }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($employee))
                            @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="form-group">
                                <label for="kode">Kode Pegawai</label>
                                <input type="number" class="form-control" id="kode" placeholder="Masukkan kode" name="kode" value="{{ @$employee->kode }}" required>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Pegawai</label>
                                <input type="text" class="form-control" id="nama" placeholder="Masukkan nama" name="nama" required value="{{ @$employee->nama }}">
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label><br>
                                <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Pria" @if (@$employee->jenis_kelamin == 'Pria') checked @endif> Pria
                                <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Wanita"  @if (@$employee->jenis_kelamin == 'Wanita') checked @endif> Wanita
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Masukkan email" name="email" required value="{{ @$employee->email }}">
                            </div>
                            <div class="form-group">
                                <label for="no_telephone">No. Telephone</label>
                                <input type="number" class="form-control" id="no_telephone" placeholder="Masukkan no_telephone" name="no_telephone" required value="{{ @$employee->no_telephone }}">
                            </div>
                            <div class="form-group">
                                <label for="position_id">Jabatan</label>
                                <select name="position_id" id="position_id" class="form-control" required>
                                    <option value="">-- jabatan --</option>
                                    @forelse ($positions as $position)
                                       <option value="{{ $position->id }}" @if (@$position->id == @$employee->position_id) selected @endif>{{ $position->name }}</option> 
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection