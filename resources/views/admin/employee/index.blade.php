@extends('layouts.main_app')
@push('css')
@include('admin.employee.css')
@endpush
@push('script')
@include('admin.employee.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pegawai</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Pegawai</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="text-right">
                            <a href="{{ route('employee.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pegawai</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Email</th>
                                    <th>No. Telephone</th>
                                    <th>Jabatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($employees as $employee)
                                    <tr>
                                        <td>{{ $employee->kode }}</td>
                                        <td>{{ $employee->nama }}</td>
                                        <td>{{ $employee->jenis_kelamin }}</td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->no_telephone }}</td>
                                        <td>{{ $employee->position->name }}</td>
                                        <td>
                                            <div class="row">
                                                <a href="{{ route('employee.edit',$employee->id) }}" class="btn btn-warning btn-sm mr-1"><i class="fa fa-edit"></i></a>
                                                <form action="{{ route('employee.destroy', $employee->id) }}" method="POST">@csrf @method('DELETE')  <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('apakah anda yakin ?')"><i class="fa fa-trash"></i></button> </form>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Data not found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection