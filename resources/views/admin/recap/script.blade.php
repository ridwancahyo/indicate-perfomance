<script src="{{ asset('assets') }}/plugins/select2/js/select2.full.min.js"></script>
<script>
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    });

    
    function submitReport() {
      $('#result-recap').empty();
      $('#result-recap').addClass('spinner-border');

      let from = $('#from').val();
      let to = $('#to').val();
      let employeeId = $('#employee_id').val();

      let url = '{{ route("recap.report", [":from",":to",":employeeId"]) }}';
      url = url.replace(':from', from);
      url = url.replace(':to', to);
      url = url.replace(':employeeId', employeeId);

      $('#result-recap').load(url, function(responseTxt, statusTxt, xhr){
      if(statusTxt == "success")
        $('#result-recap').removeClass('spinner-border');
      if(statusTxt == "error")
        alert("Error: " + xhr.status + ": " + xhr.statusText);
      });
    }
</script>