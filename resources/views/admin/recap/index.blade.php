@extends('layouts.main_app')
@push('css')
@include('admin.recap.css')
@endpush
@push('script')
@include('admin.recap.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Rekap</a></li>
                    <li class="breadcrumb-item active">Form Rekap</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="text-left">
                                <h4>Rekap</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="container">
                            <form id="FormRecap">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="from">Dari Tanggal</label>
                                        <input type="date" class="form-control" value="{{ date('Y-m-d') }}" name="from" id="from" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="to">Sampai Tanggal</label>
                                        <input type="date" class="form-control" value="{{ date('Y-m-d') }}" name="to" id="to" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pegawai</label>
                                            <select class="form-control select2bs4" name="employee_id" id="employee_id" style="width: 100%;" required>
                                              <option value="">- Pilih Karyawan</option>
                                              @foreach ($employees as $employee)
                                              <option value="{{ $employee->id }}">{{ $employee->nama }}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <button onclick="submitReport()" type="button" class="btn btn-primary">Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                            <br><br>
                            <div id="result-recap">
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection