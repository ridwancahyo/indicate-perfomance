<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <td>Tanggal</td>
            <td>Nama Pegawai</td>
            <td>Jabatan</td>
            <td>Total Score</td>
            <td>Predicate</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($recaps as $recap)
        @php
            $employee = json_decode($recap->employee_data);
            $position = json_decode($recap->position_data);
        @endphp
        <tr>
            <td>{{ date('d F Y', strtotime($recap->date)) }}</td>
            <td>{{ $employee->nama }}</td>
            <td>{{ $position->name }}</td>
            <td>{{ $recap->total_score }}</td>
            <td>{{ $recap->predicate }}</td>
        </tr>
        @endforeach
    </tbody>
</table>