@extends('layouts.main_app')
@push('css')
@include('admin.job.css')
@endpush
@push('script')
@include('admin.job.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pekerjaan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active">Pekerjaan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Pekerjaan</h3>
                    </div>
                    @php
                    $url = route('job.store');
                    if (!empty($job)) {
                        $url = route('job.update',$job->id);
                    }
                    @endphp
                    <form action="{{ $url }}" method="POST">
                        @csrf
                        @if (!empty($job))
                        @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="form-group">
                                <label for="position_id">Jabatan</label>
                                <select name="position_id" id="position_id" class="form-control" required>
                                    <option value="">-- jabatan --</option>
                                    @forelse ($positions as $position)
                                    <option value="{{ $position->id }}" @if (@$position->id == @$job->position_id) selected @endif>{{ $position->nama }}</option> 
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="indicator_id">Indicator</label>
                                <select name="indicator_id" id="indicator_id" class="form-control" required>
                                    <option value="">-- indicator --</option>
                                    @forelse ($indicators as $indicator)
                                    <option value="{{ $indicator->id }}" @if (@$indicator->id == @$job->indicator_id) selected @endif>{{ $indicator->nama }}</option> 
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kode">Kode</label>
                                <input type="number" class="form-control" id="kode" placeholder="Masukkan kode" name="kode" value="{{ @$job->kode }}" required>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Pekerjaan</label>
                                <input type="text" class="form-control" id="nama" placeholder="Masukkan nama" name="nama" required value="{{ @$job->nama }}">
                            </div>
                            <div class="form-group">
                                <label for="min_score">Minimal Score</label>
                                <input type="number" name="min_score" id="min_score" class="form-control" value="{{ @$job->min_score }}" required>
                            </div>
                            <div class="form-group">
                                <label for="max_score">Maximal Score</label>
                                <input type="number" name="max_score" id="max_score" class="form-control" value="{{ @$job->max_score }}" required>
                            </div>
                            <div class="form-group">
                                <label for="bobot_nilai">Bobot Nilai</label>
                                <input type="number" name="bobot_nilai" id="bobot_nilai" class="form-control" value="{{ @$job->bobot_nilai }}" required>
                            </div>
                        </div>
                        
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection