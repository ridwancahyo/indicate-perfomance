@extends('layouts.main_app')
@push('css')
@include('admin.assessment.css')
@endpush
@push('script')
@include('admin.assessment.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $position->name }}</h1>

            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Jabatan</a></li>
                    <li class="breadcrumb-item active">Params Data</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <p class="col-md-8">{{ $position->description }}</p>
            <p><img src="{{ asset($position->image) }}" alt="image_position" width="150px" height="100px"></p>
        </div>
        <div>

        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <div class="row">
                            <div class="text-left">
                                <h4>Indikator</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Indikator</th>
                                    <th>Bobot</th>
                                    <th>Type</th>
                                    <th>Deskripsi</th>
                                    <th>
                                        <button id="btn_add_indicator" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Indikator</button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($indicators))
                                @foreach ($indicators as $indicator)
                                <tr>
                                    <td>{{ $indicator->name }}</td>
                                    <td>{{ $indicator->bobot }} %</td>
                                    {{-- <td>{{ number_format($indicator->target) }} {{ $indicator->satuan->name }}</td> --}}
                                    <td>{{ $indicator->type }}</td>
                                    <td>{{ $indicator->description }}</td>
                                    <td>
                                        <button class="btn btn-warning btn-sm btn-edit-indicator" data-id="{{ $indicator->id }}"><i class="fa fa-edit"></i></button>
                                        <a href="{{ route('assessment.indicator.delete',$indicator->id) }}" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <div class="row">
                            <div class="text-left">
                                <h4>Proxy</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="proxy-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Indikator</th>
                                    <th>Proxy</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($indicators))
                                @foreach ($indicators as $indicator)
                                <tr>
                                    <td>{{ $indicator->name }}</td>
                                    <td>
                                        @if (!empty($indicator->proxies))
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Nama</th>
                                                <th>Bobot</th>
                                                <th>Target</th>
                                                <th>
                                                    <button class="btn btn-primary btn-sm btn-add-proxy-modal" data-id="{{ $indicator->id }}"><i class="fa fa-plus"></i> Proxy</button>
                                                </th>
                                            </tr>
                                            @foreach ($indicator->proxies as $proxy)
                                            <tr>
                                                <td>{{ $proxy->name }}</td>
                                                <td>{{ $proxy->bobot }} %</td>
                                                <td>{{ number_format($proxy->target) }} {{ $proxy->satuan->name }}</td>

                                                <td>
                                                    <button class="btn btn-warning btn-sm btn-edit-proxy" data-id="{{ $proxy->id }}"><i class="fa fa-edit"></i></button>
                                                    <a href="{{ route('assessment.proxy.delete', $proxy->id) }}" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <div class="row">
                            <div class="text-left">
                                <h4>Sub Proxy</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="subproxy-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Proxy</th>
                                    <th>Sub Proxy</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($indicators))
                                @foreach ($indicators as $indicator)
                                @if (!empty($indicator->proxies))
                                @foreach ($indicator->proxies as $proxy)
                                <tr>
                                    <td>{{ $proxy->name }}</td>
                                    <td>
                                        @if (!empty($proxy->subproxies))
                                        @php
                                            $proxy_id = $proxy->id;
                                        @endphp
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Nama</th>
                                                <th>Kode</th>
                                                <th>
                                                    <button class="btn btn-primary btn-sm btn-add-subproxy" data-id="{{ $proxy->id }}"><i class="fa fa-plus"></i> Sub Proxy</button>
                                                </th>
                                            </tr>
                                            @foreach ($proxy->subproxies as $subproxy)
                                            <tr>
                                                <td>{{ $subproxy->name }}</td>
                                                <td>{{ $subproxy->code }}</td>
                                                <td>
                                                    <button class="btn btn-warning btn-sm btn-edit-subproxy" data-id="{{ $subproxy->id }}"><i class="fa fa-edit"></i></button>
                                                    <a href="{{ route('assessment.subproxy.delete', $subproxy->id) }}" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        <table class="table">
                                            <tr>
                                                <th>Rumus</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    @foreach ($rumus as $item)
                                                        @if ($item->proxy_id == $proxy->id)
                                                            {{ $item->rumus }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <button class="btn btn-primary btn-sm btn-add-rumus" data-id="{{ $proxy->id }}">Rumus</button>
                                                </td>
                                            </tr>
                                        </table>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                @endforeach

                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@include('admin.assessment.params_data.form-indicator')
@include('admin.assessment.params_data.form-proxy')
@include('admin.assessment.params_data.form-subproxy')
@include('admin.assessment.params_data.form-rumus')
@endsection
