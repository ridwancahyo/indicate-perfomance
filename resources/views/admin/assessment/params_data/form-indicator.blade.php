<div class="modal fade" id="modal-add-indicator">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="form-indicator">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Indikator</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="position_id" id="position_id_indicator" value="{{ $position->id }}">
                    <div class="form-group">
                        <label for="name">Nama Indikator</label>
                        <input type="text" name="name" id="name_indicator" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="bobot">Bobot Indikator</label>
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" min="0" max="100" name="bobot" id="bobot_indicator" required>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-percent"></i></span>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label for="target">Target</label>
                        <div class="row">
                            <input type="number" name="target" id="target_indicator" class="form-control col-md-10" required>
                            <select name="satuan_id" id="satuan_id_indicator" class="form-control col-md-2" required>
                                @foreach ($satuans as $satuan)
                                    <option value="{{ $satuan->id }}">{{ $satuan->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label for="target">Type</label>
                        <div class="row">
                            <select name="type" id="type_indicator" class="form-control" required>
                                <option value="positive">Positive</option>
                                <option value="negative">Negative</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea name="description" id="description_indicator" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-save-indicator" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
