<div class="modal fade" id="modal-add-subproxy">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="form-subproxy">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Sub Proxy</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="proxy_id" id="proxy_id_subproxy">
                    <input type="hidden" name="subproxy_id" id="subproxy_id_subproxy">
                    <div class="form-group">
                        <label for="name">Nama Sub Proxy</label>
                        <input type="text" name="name" id="name_subproxy" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="kode">Kode Sub Proxy</label>
                        <input type="text" name="code" id="code_subproxy" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-save-subproxy" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>