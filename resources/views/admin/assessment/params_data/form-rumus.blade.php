<div class="modal fade" id="modal-add-rumus">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="form-rumus">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Rumus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="proxy_id" id="proxy_id_rumus">
                    <div class="form-group">
                        <label for="kode">Rumus</label>
                        <input type="text" name="rumus" id="rumus" class="form-control" placeholder="ex. (A*B)/C" required>
                        <small>ex. (A*B)/C</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-save-rumus" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>