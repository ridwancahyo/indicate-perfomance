<!-- DataTables  & Plugins -->
<script src="{{ asset('assets') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('assets') }}/plugins/jszip/jszip.min.js"></script>
<script src="{{ asset('assets') }}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{ asset('assets') }}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
    // $(function () {
    //     $("#example1").DataTable({
    //         "responsive": true, "lengthChange": false, "autoWidth": false,
    //         // "buttons": ["csv", "excel", "pdf", "print"]
    //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    // });
    // $(function () {
    //     $("#subproxy-table").DataTable({
    //         "responsive": true, "lengthChange": false, "autoWidth": false,
    //         // "buttons": ["csv", "excel", "pdf", "print"]
    //     }).buttons().container().appendTo('#subproxy-table .col-md-6:eq(0)');

    // });

    // $(function () {
    //     $("#proxy-table").DataTable({
    //         "responsive": true, "lengthChange": false, "autoWidth": false,
    //         // "buttons": ["csv", "excel", "pdf", "print"]
    //     }).buttons().container().appendTo('#proxy-table .col-md-6:eq(0)');

    // });

    $('#btn_add_indicator').on('click', function (e) {
        let url = "{{ route('assessment.indicator.save') }}";
        $('#form-indicator').attr('action',url);
        $('#modal-add-indicator').modal('show');
    });

    $('.btn-edit-indicator').on('click', function (e) {
        let indicator_id = $(this).data('id');
        var url = '{{ route("assessment.indicator.update", ":id") }}';
        url = url.replace(':id', indicator_id);
        $('#form-indicator').attr('action',url);

        $.get("/assessment/indicator/get/"+indicator_id, function(data, status){
            $('#name_indicator').val(data.data.name);
            $('#bobot_indicator').val(data.data.bobot);
            // $('#target_indicator').val(data.data.target);
            $('#type_indicator').val(data.data.type).attr('selected',true);
            $('#satuan_id_indicator').val(data.data.satuan_id).attr('selected',true);
            $('#description_indicator').val(data.data.description);
            $('#modal-add-indicator').modal('show');
        });
    });

    $('.btn-add-proxy-modal').on('click', function (e) {
        let indicator_id = $(this).data('id');
        $('#indicator_id_proxy').val(indicator_id);
        var url = '{{ route("assessment.proxy.save") }}';
        $('#form-proxy').attr('action',url);
        $('#modal-add-proxy').modal('show');
    });

    $('.btn-edit-proxy').on('click', function (e) {
        let proxy_id = $(this).data('id');
        $('#proxy_id_proxy').val(proxy_id);
        var url = '{{ route("assessment.proxy.update", ":id") }}';
        url = url.replace(':id', proxy_id);
        $('#form-proxy').attr('action',url);
        $.get("/assessment/proxy/get/"+proxy_id, function(data, status){
            $('#name_proxy').val(data.data.name);
            $('#bobot_proxy').val(data.data.bobot);
            $('#target_proxy').val(data.data.target);
            $('#indicator_id_proxy').val(data.data.indicator_id);
            $('#modal-add-proxy').modal('show');
        });

    });

    $('.btn-add-subproxy').on('click', function (e) {
        let proxy_id = $(this).data('id');
        $('#proxy_id_subproxy').val(proxy_id);
        var url = '{{ route("assessment.subproxy.save") }}';
        $('#form-subproxy').attr('action',url);
        $('#modal-add-subproxy').modal('show');
    });

    $('.btn-edit-subproxy').on('click', function (e) {
        let subproxy_id = $(this).data('id');
        $('#subproxy_id_subproxy').val(subproxy_id);
        var url = '{{ route("assessment.subproxy.update", ":id") }}';
        url = url.replace(':id', subproxy_id);
        $('#form-subproxy').attr('action',url);
        $.get("/assessment/subproxy/get/"+subproxy_id, function(data, status){
            $('#name_subproxy').val(data.data.name);
            $('#code_subproxy').val(data.data.code);
            $('#proxy_id_subproxy').val(data.data.proxy_id);
            $('#modal-add-subproxy').modal('show');
        });

    });

    $('.btn-add-rumus').on('click', function (e) {
        let proxy_id = $(this).data('id');
        $('#proxy_id_rumus').val(proxy_id);
        var url = '{{ route("assessment.subproxy.rumus.add") }}';
        $('#form-rumus').attr('action',url);
        $.get("/assessment/subproxy/rumus/get/"+proxy_id, function(data, status){
            if (data.data) {
                $('#rumus').val(data.data.rumus);
                $('#proxy_id_rumus').val(data.data.proxy_id);
            }else{
                $('#rumus').val('');
                $('#proxy_id_rumus').val(proxy_id);
            }
            $('#modal-add-rumus').modal('show');
        });
    });


</script>
