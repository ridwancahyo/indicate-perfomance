@extends('layouts.main_app')
@push('css')
@include('admin.assessment.css')
@endpush
@push('script')
@include('admin.assessment.script')
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Penilaian</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Penilaian</a></li>
                    <li class="breadcrumb-item active">Pegawai</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <div class="row">
                            <div class="text-left">
                                <h4>Pegawai</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Email</th>
                                    <th>No. Telephone</th>
                                    <th>Jabatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($employees))
                                    @foreach ($employees as $employee)
                                        <tr>
                                            <td>{{ $employee->kode }}</td>
                                            <td>{{ $employee->nama }}</td>
                                            <td>{{ $employee->jenis_kelamin }}</td>
                                            <td>{{ $employee->email }}</td>
                                            <td>{{ $employee->no_telephone }}</td>
                                            <td>{{ $employee->position->name }}</td>
                                            <td>
                                                <a href="{{ route('calculate.index', $employee->id) }}" class="btn btn-success btn-sm"><i class="fa fa-book"></i> Penilaian</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection