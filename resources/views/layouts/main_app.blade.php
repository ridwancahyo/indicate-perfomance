<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Indicate Performance</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @include('layouts.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    
    <!-- Navbar -->
    @include('layouts.navbar')
    <!-- /.navbar -->
    
    <!-- Main Sidebar Container -->
    @include('layouts.sidebar')
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @if (Session::has('alert'))
      @php
          $alert = Session::get('alert');
          $simbol = 'ban';
          $title = 'Failed';
          if ($alert == 'success') {
            $simbol = 'check';
            $title = 'Success';
          }
      @endphp
      <div class="card-body">
        <div class="alert alert-{{ $alert }} alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><i class="icon fas fa-{{ $simbol }}"></i> {{ $title }}!</h5>
          {{ Session::get('alert-message') }}
        </div>
      </div>
      @endif
      @yield('content')
      <!-- /.content -->
    </div>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->
  @include('layouts.script')
</body>
</html>
