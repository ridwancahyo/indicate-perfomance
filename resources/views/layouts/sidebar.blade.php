<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <span class="brand-text font-weight-light">Indicate Performance</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        </div>
        <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('dashboard') }}" class="nav-link @if(Request::is('dashboard')) active  @endif">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('assessment.employee') }}" class="nav-link @if(Request::is('assessment/employee*') || Request::is('calculate*')) active  @endif">
              <i class="nav-icon fas fa-check"></i>
              <p>
                Penilaian
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('recap.index') }}" class="nav-link @if(Request::is('recap*')) active  @endif">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Rekap
              </p>
            </a>
          </li>
          <li class="nav-item @if(Request::is('master-data/employee*') || Request::is('master-data/position*') || Request::is('master-data/indicator*') || Request::is('master-data/proxy*') || Request::is('master-data/subproxy*') || Request::is('master-data/satuan*')) menu-open  @endif">
            <a href="#" class="nav-link @if(Request::is('master-data/employee*') || Request::is('master-data/position*') || Request::is('master-data/indicator*') || Request::is('master-data/proxy*') || Request::is('master-data/subproxy*') || Request::is('master-data/satuan*')) active  @endif">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('position.index') }}" class="nav-link @if(Request::is('master-data/position*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jabatan</p>
                </a>
              </li>
              {{--  <li class="nav-item">
                <a href="{{ route('indicator.index') }}" class="nav-link @if(Request::is('master-data/indicator*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Indikator</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('proxy.index') }}" class="nav-link @if(Request::is('master-data/proxy*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Proxy</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('subproxy.index') }}" class="nav-link @if(Request::is('master-data/subproxy*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sub Proxy</p>
                </a>
              </li>  --}}
              <li class="nav-item">
                <a href="{{ route('employee.index') }}" class="nav-link @if(Request::is('master-data/employee*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pegawai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('satuan.index') }}" class="nav-link @if(Request::is('master-data/satuan*')) active  @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Satuan</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>