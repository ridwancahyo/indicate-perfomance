<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::group(['prefix' => 'master-data'], function () {
    Route::resource('/employee', 'EmployeeController')->middleware('auth');

    //position route
    Route::resource('/position', 'PositionController')->middleware('auth');
    Route::get('/position/get-indicator/{position_id}', 'PositionController@GetIndicator')->middleware('auth')->name('position.add_indicator');
    Route::post('/position/add-indicator', 'PositionController@AddIndicator')->middleware('auth')->name('position.addIndicator.post');
    Route::delete('position/delete-indicator/{indicator_id}', 'PositionController@DeleteIndicator')->middleware('auth')->name('position.deleteIndicator');
    Route::get('/position/{position_id}/params_data', 'AssessmentController@IndexParamsData')->name('position.indexparamsdata');

    Route::resource('/indicator', 'IndicatorController')->middleware('auth');
    Route::get('/indicator/get-proxy/{indicator_id}', 'IndicatorController@GetProxy')->middleware('auth')->name('indicator.add_proxy');
    Route::post('/indicator/add-proxy/', 'IndicatorController@AddProxy')->middleware('auth')->name('indicator.addProxy.post');
    Route::delete('indicator/delete-proxy/{proxy_id}', 'IndicatorController@DeleteProxy')->middleware('auth')->name('indicator.deleteProxy');

    // Route::resource('/job', 'JobController')->middleware('auth');
    Route::resource('/proxy', 'ProxyController')->middleware('auth');
    Route::resource('/subproxy', 'SubProxyController')->middleware('auth');

    //satuan route
    Route::resource('/satuan', 'SatuanController')->middleware('auth');


});

Route::group(['prefix' => 'assessment','middleware' => 'auth'], function () {
    Route::get('/employee', 'AssessmentController@Employee')->name('assessment.employee');
    //indicator
    Route::post('/indicator/save', 'AssessmentController@IndicatorSave')->name('assessment.indicator.save');
    Route::post('/indicator/update/{indicator_id}', 'AssessmentController@IndicatorUpdate')->name('assessment.indicator.update');
    Route::get('/indicator/get/{indicator_id}', 'AssessmentController@GetIndicator')->name('assessment.indicator.get');
    Route::get('/indicator/delete/{indicator_id}','AssessmentController@IndicatorDelete')->name('assessment.indicator.delete');

    //proxy
    Route::post('/proxy/save', 'AssessmentController@ProxySave')->name('assessment.proxy.save');
    Route::post('/proxy/update/{proxy_id}', 'AssessmentController@ProxyUpdate')->name('assessment.proxy.update');
    Route::get('/proxy/get/{proxy_id}', 'AssessmentController@GetProxy')->name('assessment.proxy.get');
    Route::get('/proxy/delete/{proxy_id}','AssessmentController@ProxyDelete')->name('assessment.proxy.delete');

    //subproxy
    Route::post('/subproxy/save', 'AssessmentController@SubProxySave')->name('assessment.subproxy.save');
    Route::post('/subproxy/update/{subproxy_id}', 'AssessmentController@SubProxyUpdate')->name('assessment.subproxy.update');
    Route::get('/subproxy/get/{subproxy_id}', 'AssessmentController@GetSubProxy')->name('assessment.subproxy.get');
    Route::get('/subproxy/delete/{subproxy_id}','AssessmentController@SubProxyDelete')->name('assessment.subproxy.delete');
    Route::post('/subproxy/rumus/save', 'AssessmentController@AddRumus')->name('assessment.subproxy.rumus.add');
    Route::get('/subproxy/rumus/get/{proxy_id}', 'AssessmentController@GetRumus')->name('assessment.subproxy.rumus.get');


});

Route::group(['prefix' => 'calculate','middleware' => 'auth'], function () {
    Route::get('/employee/{employee_id}', 'CalculateController@index')->name('calculate.index');
    Route::post('/subproxy', 'CalculateController@CalculateSubproxy')->name('calculate.subproxy');
});

Route::group(['prefix' => 'recap','middleware' => 'auth'], function () {
    Route::get('/', 'RecapController@index')->name('recap.index');
    Route::post('/save', 'RecapController@store')->name('recap.store');
    Route::get('/get-report/{from}/{to}/{employee_id}', 'RecapController@report')->name('recap.report');
});
