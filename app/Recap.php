<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recap extends Model
{
    protected $fillable = [
        'user_id', 'date', 'employee_id', 'position_id', 'total_score', 'predicate',
        'employee_data', 'position_data', 'score_data'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }
}
