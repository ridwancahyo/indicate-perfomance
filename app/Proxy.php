<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $fillable = [
        'name','bobot','indicator_id','user_id','satuan_id','target'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function indicator()
    {
        return $this->hasOne('App\Indicator', 'id', 'indicator_id');
    }

    public function subproxies()
    {
        return $this->hasMany('App\SubProxy', 'proxy_id', 'id');
    }
    
    public function rumuss()
    {
        return $this->hasOne('App\RumusSubProxy', 'proxy_id', 'id');
    }

    public function satuan()
    {
        return $this->hasOne('App\Satuan', 'id', 'satuan_id');
    }
}
