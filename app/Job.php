<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'kode','nama','min_score','max_score','bobot_nilai','position_id','indicator_id'
    ];

    public function position()
    {
        return $this->hasOne('App\Position', 'id', 'position_id');
    }

    public function indicator()
    {
        return $this->hasOne('App\Indicator', 'id', 'indicator_id');
    }
}
