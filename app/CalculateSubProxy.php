<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculateSubProxy extends Model
{
    protected $fillable = [
        'param_data','rumus','hasil','proxy_id','user_id','employee_id','score_proxy','recap'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function scopeNoRecap($query)
    {
        return $query->where('recap', 0);
    }

    public function proxy()
    {
        return $this->hasOne('App\Proxy', 'id', 'proxy_id');
    }
}
