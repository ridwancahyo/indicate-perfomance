<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
        'name','user_id','image','description'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'position_id','id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'position_id', 'id');
    }

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function indicators()
    {
        return $this->hasMany('App\Indicator', 'position_id', 'id');
    }

    
}
