<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalScore extends Model
{
    protected $fillable = [
        'user_id', 'employee_id', 'total_score', 'predicate', 'recap'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function scopeNoRecap($query)
    {
        return $query->where('recap', 0);
    }
}
