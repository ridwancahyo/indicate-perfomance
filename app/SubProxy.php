<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubProxy extends Model
{
    protected $fillable = [
        'name','code','proxy_id','user_id'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function proxy()
    {
        return $this->hasOne('App\Proxy', 'id', 'proxy_id');
    }

    public function rumuss()
    {
        return $this->hasOne('App\RumusSubProxy', 'proxy_id', 'proxy_id');
    }
}
