<?php

namespace App\Http\Requests\SubProxy;

use Illuminate\Foundation\Http\FormRequest;

class SubProxyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'proxy_id' => 'required',
            'name' => 'required',
            'code' => 'required',
        ];
    }
}
