<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position_id' => 'required',
            'indicator_id' => 'required',
            'kode' => 'required',
            'nama' => 'required',
            'min_score' => 'required',
            'max_score' => 'required',
            'bobot_nilai' => 'required',
        ];
    }
}
