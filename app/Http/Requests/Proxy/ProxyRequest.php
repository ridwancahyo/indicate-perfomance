<?php

namespace App\Http\Requests\Proxy;

use Illuminate\Foundation\Http\FormRequest;

class ProxyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'indicator_id' => 'required',
            'name' => 'required',
            'target' => 'required',
            'satuan_id' => 'required',
            // 'bobot' => 'required|numeric'
        ];
    }
}
