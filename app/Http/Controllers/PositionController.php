<?php

namespace App\Http\Controllers;

use App\Http\Requests\Position\PositionRequest;
use App\Http\Requests\Position\AddIndicatorPositionRequest;
use App\Position;
use App\Indicator;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::with('indicators')->userLogin()->get();
        return view('admin.position.index', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.position.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PositionRequest $request)
    {
        try {
            if (!$request->file('image')) {
                createSession('danger','Photo harus di isi !!!');
                return back();
            }

            $imagePositionLocation = "images/position/".user_login()->id."/";
            $imagePosition = $request->file('image');
            $imageName = time().$imagePosition->getClientOriginalName();
            $imagePosition->move(public_path($imagePositionLocation), $imageName);
            
            
            $position = Position::create(['name' => $request->name, 
                                          'user_id' => user_login()->id, 
                                          'description' => $request->description,
                                          'image' => $imagePositionLocation.$imageName]);

            createSession('success','Data berhasil disimpan ...');
            return redirect(route('position.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::findOrFail($id);
        return view('admin.position.form', compact('position'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionRequest $request, $id)
    {
        try {
            $position = Position::findOrFail($id);

            if ($request->file('image')) {

                //remove image
                unlink(public_path($position->image));

                $imagePositionLocation = "images/position/".user_login()->id."/";
                $imagePosition = $request->file('image');
                $imageName = time().$imagePosition->getClientOriginalName();
                $imagePosition->move(public_path($imagePositionLocation), $imageName);

                $position->image = $imagePositionLocation.$imageName;
            }

            
            $position->name = $request->name;
            $position->description = $request->description;
            $position->user_id = user_login()->id;
            $position->save();

            createSession('success','Data berhasil disimpan ...');
            return redirect(route('position.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $position = Position::findOrFail($id);

            //remove image
            unlink(public_path($position->image));

            $position->delete();

            createSession('success','Data berhasil dihapus ...');
            return redirect(route('position.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }

    }

    public function GetIndicator($position_id)
    {
        try {
            $position = Position::findOrFail($position_id);
            $indicators = Indicator::where('position_id',$position_id)->get();
            $otherIndicators = Indicator::whereNull('position_id')->get();
            return view('admin.position.get-indicator', compact('indicators','otherIndicators','position'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function AddIndicator(AddIndicatorPositionRequest $request)
    {
        try {
            $indicators = Indicator::where('position_id',$request->position_id)->get();
            $totalBobot = 0;
            if (!empty($indicators)) {
                foreach ($indicators as $indicator) {
                    $totalBobot += $indicator->bobot;
                }
            }

            $totalSumAll = $totalBobot + $request->bobot;
            if ($totalSumAll > 100) {
                return "bobot melebihi 100 %";
            }

            $indicator = Indicator::findOrFail($request->indicator_id);
            $indicator->position_id = $request->position_id;
            $indicator->bobot = $request->bobot;
            $indicator->save();

            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function DeleteIndicator($indicator_id)
    {
        try {
            $indicator = Indicator::findOrFail($indicator_id);
            $indicator->position_id = null;
            $indicator->bobot = null;
            $indicator->save();

            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }
}
