<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Indicator;
use App\Employee;
use App\Proxy;
use App\RumusSubProxy;
use App\CalculateSubproxy;
use App\ScoreIndicator;
use App\TotalScore;
use App\Recap;
use DB;

class CalculateController extends Controller
{
    public function index($employee_id)
    {
        try {
            $RecapEmployee = Recap::userLogin()->where('employee_id', $employee_id)->where('date', date('Y-m-d'))->first();
            if (!empty($RecapEmployee)) {
                createSession('danger','Data pegawai untuk hari ini sudah di recap..');
                return back();
            }
            $employee = Employee::with('position')->userLogin()->where('id', $employee_id)->first();
            $indicators = Indicator::with('proxies.subproxies.rumuss')->where('position_id', $employee->position_id)->userLogin()->get();
            $list_lagi = getListProxyId($indicators);
            $rumus = RumusSubProxy::whereIn('proxy_id',$list_lagi)->userLogin()->get();
            $result = CalculateSubproxy::noRecap()->userLogin()->whereIn('proxy_id', $list_lagi)->where('employee_id', $employee->id)->get();
            $totalScore = TotalScore::noRecap()->userLogin()->where('employee_id', $employee->id)->first();
            $disabledCalculate = (@$totalScore->recap == 1) ? true : false;
            // $disabledCalculate = true;
            // dd($indicators);
            return view('admin.calculate.index', compact('indicators','rumus','result', 'totalScore', 'disabledCalculate', 'employee'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function CalculateSubproxy(Request $request)
    {
        try {
            DB::beginTransaction();
            $string = $request->rumus;
            if (empty($string)) {
                DB::rollback();
                createSession('danger','Terjadi kesalahan, mohon untuk cek rumus atau data yang di input !');
                return back();
            }
            preg_match_all("(\w+)", $string, $matches);

            foreach ($matches[0] as $key => $value) {
                try {
                    $string = str_replace($value,$request->nilai[$value],$string);
                } catch (\Throwable $th) {
                    DB::rollback();
                    createSession('danger','Terjadi kesalahan, mohon untuk cek rumus atau data yang di input !');
                    return back();
                }
            }
            eval( '$hasil = (' . $string. ');' );

            $indicator = Indicator::findOrFail($request->indicator_id);
            //get data proxy
            $proxy = Proxy::findOrFail($request->proxy_id);
            $bobot_proxy = $proxy->bobot;

            //rumus nilai proxy
            $score_proxy = 0;
            if ($indicator->type == "positive") {
                $score_proxy = ($hasil/$proxy->target);
                if ($score_proxy > 1) {
                    $score_proxy = 1;
                }
            }else if ($indicator->type == "negative") {
                $score_proxy = ($hasil/$proxy->target);
                $target = $proxy->target;
                if ($score_proxy > 1) {
                    $score_proxy = (1-($hasil-$target)/$target);
                }
            }
            // //score proxy
            // $score_proxy = ($bobot_proxy/100)*$hasil;
            // $score_proxy = $score_proxy * 100;

            //insert database
            $saveData = CalculateSubproxy::updateOrCreate(
                ['user_id' => user_login()->id, 'proxy_id' => $request->proxy_id, 'employee_id' => $request->employee_id, 'recap' => 0],
                ['param_data' => json_encode($request->nilai), 'hasil' => $hasil, 'rumus' => $request->rumus, 'score_proxy' => $score_proxy*100]
            );

            //update score indicator
            $proxyIDs = Proxy::userLogin()->where('indicator_id', $request->indicator_id)->get()->pluck(['id']);
            $scoreProxies = CalculateSubproxy::with('proxy')->whereIn('proxy_id',$proxyIDs)->noRecap()->userLogin()->get();
            $scoreIndicator = 0;
            if (!empty($scoreProxies)) {
                foreach ($scoreProxies as $scoreProxy) {
                    $bobot = $scoreProxy->proxy->bobot/100;
                    $score = $scoreProxy->score_proxy/100;
                    $result = ($bobot*$score)*100;
                    $scoreIndicator += $result;
                    // $scoreIndicator += $scoreProxy->score_proxy;
                }
            }
            // $scoreIndicator = ($indicator->bobot/100) * $scoreIndicator;

            //insert to score indicator
            $saveIndicatorScore = ScoreIndicator::updateOrCreate(
                ['indicator_id' => $indicator->id, 'employee_id' => $request->employee_id, 'user_id' => user_login()->id, 'recap' => 0],
                ['score' => $scoreIndicator]
            );

            //insert total score
            $employee = Employee::find($request->employee_id);
            $getIndicators = Indicator::userLogin()->where('position_id', $employee->position_id)->get()->pluck(['id']);
            $getScoreIndicators = ScoreIndicator::with('indicator')->noRecap()->userLogin()->whereIn('indicator_id', $getIndicators)->where('employee_id', $employee->id)->get();
            $totalScore = 0;
            if (!empty($getScoreIndicators)) {
                foreach ($getScoreIndicators as $scoreIndicator) {
                    $bobot = $scoreIndicator->indicator->bobot/100;
                    $score = $scoreIndicator->score/100;
                    $result = ($bobot*$score)*100;
                    $totalScore += $result;
                }
            }
            $predicate = $this->getPredicate($totalScore);
            $saveTotalScore = TotalScore::updateOrCreate(
                ['employee_id' => $employee->id, 'user_id' => user_login()->id, 'recap' => 0],
                ['total_score' => $totalScore, 'predicate' => $predicate]
            );
            DB::commit();
            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    private function getPredicate($totalScore)
    {
        try {
            $predicate = '-';
            if ($totalScore >= 91 && $totalScore <= 100) {
                $predicate = 'A';
            }elseif ($totalScore >= 81 && $totalScore <= 90.99) {
                $predicate = 'B';
            }elseif ($totalScore >= 71 && $totalScore <= 80.99) {
                $predicate = 'C';
            }elseif ($totalScore >= 61 && $totalScore <= 70.99) {
                $predicate = 'D';
            }elseif ($totalScore >= 0 && $totalScore <= 60.99) {
                $predicate = 'E';
            }else{
                $predicate = '-';
            }
            return $predicate;
        } catch (\Throwable $th) {
            return '-';
        }
    }
}
