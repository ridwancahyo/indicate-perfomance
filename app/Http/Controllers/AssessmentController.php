<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Position;
use App\SubProxy;
use App\Proxy;
use App\Indicator;
use App\Satuan;
use App\RumusSubProxy;
use App\Http\Requests\Indicator\IndicatorRequest;
use App\Http\Requests\Proxy\ProxyRequest;
use App\Http\Requests\SubProxy\SubProxyRequest;
use DB;
use Session;

class AssessmentController extends Controller
{
    public function Employee()
    {
        $employees = Employee::userLogin()->get();
        return view('admin.assessment.employee.index', compact('employees'));
    }

    public function IndexParamsData($position_id)
    {
        try {
            $position = Position::findOrFail($position_id);
            $dataIndicators = Indicator::userLogin()->get();
            $indicators = Indicator::with('proxies.subproxies.rumuss')->where('position_id', $position->id)->get();
            $satuans = Satuan::userLogin()->statusActived()->get();
            $list_lagi = getListProxyId($indicators);
            $rumus = RumusSubProxy::whereIn('proxy_id',$list_lagi)->get();
            // dd($rumus);
            return view('admin.assessment.params_data.index', compact('position','indicators','dataIndicators','satuans','rumus'));
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function GetIndicator($indicator_id)
    {
        try {
            $indicator = Indicator::findOrFail($indicator_id);
            return response()->json([
                'data' => $indicator,
                'success' => true
            ], 200);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function IndicatorSave(IndicatorRequest $request)
    {
        try {
            $indicators = Indicator::where('position_id', $request->position_id)->get();
            $total = 0;
            if (!empty($indicators)) {
                foreach ($indicators as $indicator) {
                    $total += $indicator->bobot;
                }
            }

            $totalAll = $total + $request->bobot;
            if ($totalAll > 100) {
                createSession('danger','total semua bobot melebihi 100 %');
                return back();
            }
            Indicator::create([
                'name' => $request->name,
                'bobot' => $request->bobot,
                // 'target' => $request->target,
                'target' => 0,
                'satuan_id' => $request->satuan_id,
                'description' => $request->description,
                'position_id' => $request->position_id,
                'user_id' => user_login()->id,
            ]);

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function IndicatorUpdate(IndicatorRequest $request, $indicator_id)
    {
        try {
            $indicators = Indicator::where('position_id', $request->position_id)->where('id','!=',$indicator_id)->get();
            $total = 0;
            if (!empty($indicators)) {
                foreach ($indicators as $indicator) {
                    $total += $indicator->bobot;
                }
            }
            $totalAll = $total + $request->bobot;
            if ($totalAll > 100) {
                createSession('danger','total semua bobot melebihi 100 %');
                return back();
            }
            $indicator = Indicator::findOrFail($indicator_id);
            $indicator->name = $request->name;
            $indicator->position_id = $request->position_id;
            $indicator->bobot = $request->bobot;
            $indicator->target = $request->target;
            $indicator->satuan_id = $request->satuan_id;
            $indicator->description = $request->description;
            $indicator->save();

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function IndicatorDelete($indicator_id)
    {
        try {
            DB::transaction(function () use($indicator_id) {
                $indicator = Indicator::findOrFail($indicator_id);
                $proxies = $indicator->proxies->pluck('id');
                $subproxiesDelete = SubProxy::whereIn('proxy_id',$proxies)->delete();
                $proxiesDelete = Proxy::whereIn('id',$proxies)->delete();
                $indicator->delete();
            });

            createSession('success','Data berhasil dihapus ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function ProxySave(ProxyRequest $request)
    {
        try {
            $proxies = Proxy::where('indicator_id', $request->indicator_id)->get();
            $total = 0;
            if (!empty($proxies)) {
                foreach ($proxies as $proxy) {
                    $total += $proxy->bobot;
                }
            }

            $totalAll = $total + $request->bobot;
            if ($totalAll > 100) {
                createSession('danger','total semua bobot melebihi 100 %');
                return back();
            }
            Proxy::create([
                'name' => $request->name,
                'bobot' => $request->bobot,
                'target' => $request->target,
                'satuan_id' => $request->satuan_id,
                'indicator_id' => $request->indicator_id,
                'user_id' => user_login()->id,
            ]);

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function GetProxy($proxy_id)
    {
        try {
            $proxy = Proxy::findOrFail($proxy_id);
            return response()->json([
                'data' => $proxy,
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'data' => $th->getMessage(),
                'success' => false,
            ], 500);
        }
    }

    public function ProxyUpdate(ProxyRequest $request)
    {
        try {
            $proxies = Proxy::where('indicator_id',$request->indicator_id)->where('id','!=',$request->proxy_id)->get();
            $total = 0;
            if (!empty($proxies)) {
                foreach ($proxies as $proxy) {
                    $total += $proxy->bobot;
                }
            }
            $totalAll = $total + $request->bobot;
            if ($totalAll > 100) {
                createSession('danger','total semua bobot melebihi 100 %');
                return back();
            }

            $proxy = Proxy::findOrFail($request->proxy_id);
            $proxy->name = $request->name;
            $proxy->bobot = $request->bobot;
            $proxy->target = $request->target;
            $proxy->satuan_id = $request->satuan_id;
            $proxy->save();

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function ProxyDelete($proxy_id)
    {
        try {
            DB::transaction(function () use($proxy_id) {
                $subproxies = SubProxy::where('proxy_id',$proxy_id)->delete();
                $proxy = Proxy::destroy($proxy_id);
            });

            createSession('success','Data berhasil dihapus ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function SubProxySave(SubProxyRequest $request)
    {
        try {
            //check code
            $subProxy = SubProxy::where('proxy_id', $request->proxy_id)->where('code',$request->code)->userLogin()->first();
            if (strtolower($request->code) ==  strtolower(@$subProxy->code)) {
                createSession('danger', 'Kode Sub Proxy sudah digunakan...');
                return back();
            }
            SubProxy::create([
                'name' => $request->name,
                'code' => strtoupper($request->code),
                'proxy_id' => $request->proxy_id,
                'user_id' => user_login()->id,
            ]);

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function GetSubProxy($subproxy_id)
    {
        try {
            $subproxy = SubProxy::findOrFail($subproxy_id);

            return response()->json([
                'data' => $subproxy,
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'data' => $th->getMessage(),
                'success' => false,
            ], 500);
        }
    }

    public function SubProxyUpdate(SubProxyRequest $request)
    {
        try {
            //check code
            $subproxy = SubProxy::findOrFail($request->subproxy_id);
            //check code
            $subProxy2 = SubProxy::where('proxy_id', $subproxy->proxy_id)->where('code',$request->code)->userLogin()->first();
            if (strtolower($request->code) ==  strtolower(@$subProxy2->code)) {
                if (@$subproxy->id != @$subProxy2->id) {
                    createSession('danger', 'Kode Sub Proxy sudah digunakan...');
                    return back();
                }
            }
            $subproxy->name = $request->name;
            $subproxy->code = strtoupper($request->code);
            $subproxy->save();

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function SubProxyDelete($subproxy_id)
    {
        try {
            $subproxy = SubProxy::destroy($subproxy_id);

            createSession('success','Data berhasil dihapus ...');
            return back();
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    public function AddRumus(Request $request)
    {
        try {
            $rumus = strtoupper($request->rumus);
            $proxy_id = $request->proxy_id;

            $saveData = RumusSubProxy::updateOrCreate(
                ['user_id' => user_login()->id, 'proxy_id' => $proxy_id],
                ['rumus' => $rumus]
            );

            createSession('success','Data berhasil disimpan ...');
            return back();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function GetRumus($proxy_id)
    {
        try {
            $getRumus = RumusSubProxy::where('proxy_id', $proxy_id)->userLogin()->first();
            return response()->json([
                'data' => $getRumus,
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'data' => $th->getMessage(),
                'success' => false,
            ], 500);
        }
    }

}
