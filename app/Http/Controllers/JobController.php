<?php

namespace App\Http\Controllers;

use App\Http\Requests\Job\JobRequest;
use App\Job;
use App\Position;
use App\Indicator;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all();
        return view('admin.job.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Position::all();
        $indicators = Indicator::all();
        return view('admin.job.form', compact('positions','indicators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        try {
            $job = Job::create($request->all());

            return redirect(route('job.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $positions = Position::all();
        $indicators = Indicator::all();
        $job = Job::findOrFail($id);
        return view('admin.job.form', compact('positions','indicators','job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {
        try {
            $job = Job::findOrFail($id);

            $job->position_id = $request->position_id;
            $job->indicator_id = $request->indicator_id;
            $job->kode = $request->kode;
            $job->nama = $request->nama;
            $job->min_score = $request->min_score;
            $job->max_score = $request->max_score;
            $job->bobot_nilai = $request->bobot_nilai;
            $job->save();

            return redirect(route('job.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $job = Job::destroy($id);

            return redirect(route('job.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
