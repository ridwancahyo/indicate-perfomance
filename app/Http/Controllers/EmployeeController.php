<?php

namespace App\Http\Controllers;

use App\Http\Requests\employee\EmployeeRequest;
use App\Employee;
use App\Position;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::userLogin()->get();
        return view('admin.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Position::userLogin()->get();
        return view('admin.employee.form', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        try {
            $employee = Employee::create([
                'kode' => $request->kode,
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'no_telephone' => $request->no_telephone,
                'position_id' => $request->position_id,
                'user_id' => user_login()->id,
            ]);

            createSession('success','Data berhasil disimpan ...');
            return redirect(route('employee.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $positions = Position::userLogin()->get();
        return view('admin.employee.form', compact('employee','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->kode = $request->kode;
            $employee->nama = $request->nama;
            $employee->jenis_kelamin = $request->jenis_kelamin;
            $employee->email = $request->email;
            $employee->no_telephone = $request->no_telephone;
            $employee->user_id = user_login()->id;
            $employee->save();

            createSession('success','Data berhasil disimpan ...');
            return redirect(route('employee.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = Employee::destroy($id);
            
            createSession('success','Data berhasil dihapus ...');
            return redirect(route('employee.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }
}
