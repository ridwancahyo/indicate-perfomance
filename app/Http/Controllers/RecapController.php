<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Position;
use App\TotalScore;
use App\Proxy;
use App\ScoreIndicator;
use App\CalculateSubProxy;
use App\Recap;
use DB;

class RecapController extends Controller
{
    public function index()
    {
        $employees = Employee::userLogin()->get();
        return view('admin.recap.index',[
            'employees' => $employees
        ]);
    }

    public function store(Request $req)
    {
        try {
            DB::beginTransaction();
            $employeeId = $req->employee_id;
            $employee = Employee::findOrFail($employeeId);
            $employeeData = json_encode($employee);

            $position = Position::findOrFail($employee->position_id);
            $positionData = json_encode($position);

            $scoreData = [];
            $totalScore = TotalScore::userLogin()->where('employee_id', $employeeId)->noRecap()->first();
            $totalIndicators = ScoreIndicator::noRecap()->userLogin()->where('employee_id', $employeeId)->get();
            $scoreData['total_score'] = $totalScore->total_score;
            $scoreData['predicate'] = $totalScore->predicate;
            $indicatorIDs = [];
            foreach ($totalIndicators as $totalIndicator) {
                $indicatorIDs[] = $totalIndicator->indicator_id;
                $proxyIDs = Proxy::userLogin()->where('indicator_id',$totalIndicator->indicator_id)->get()->pluck(['id'])->toArray();
                foreach ($proxyIDs as $idProxy) {
                    $scoreSubProxy = CalculateSubProxy::noRecap()->userLogin()->where('proxy_id', $idProxy)->first();
                    $param_data = json_decode($scoreSubProxy->param_data, true);
                    $scoreData['indicators'][] = [
                        'id' => $totalIndicator->indicator_id,
                        'score' => $totalIndicator->score,
                        'proxies' => [
                            'id' => $idProxy,
                            'score' => $scoreSubProxy->score_proxy,
                            'subproxies' => [
                                'rumus' => $scoreSubProxy->rumus,
                                'score' => $scoreSubProxy->hasil,
                                'param_data' => $param_data,
                            ],
                        ],
                    ];
                }
            }
            $scoreData = json_encode($scoreData);

            //save recap
            $recap = Recap::create([
                'user_id' => user_login()->id,
                'date' => date('Y-m-d'),
                'employee_id' => $employeeId,
                'position_id' => $position->id,
                'total_score' => $totalScore->total_score,
                'predicate' => $totalScore->predicate,
                'employee_data' => $employeeData,
                'position_data' => $positionData,
                'score_data' => $scoreData,
            ]);

            //update data to recap
            $totalScore->recap = 1;
            $totalScore->save();
            ScoreIndicator::noRecap()->userLogin()->where('employee_id', $employeeId)->update(['recap' => 1]);
            CalculateSubProxy::noRecap()->userLogin()->where('employee_id', $employeeId)->update(['recap' => 1]);
            DB::commit();
            createSession('success','Data Rekap berhasil disimpan ...');
            return redirect(route('assessment.employee'));
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function report(Request $req)
    {
        try {
            $recaps = Recap::userLogin()->where('employee_id', $req->employee_id)->where('date','>=',$req->from)->where('date','<=',$req->to)->get();
            return view('admin.recap.report.index', compact('recaps'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
