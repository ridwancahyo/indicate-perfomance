<?php

namespace App\Http\Controllers;

use App\Http\Requests\Proxy\ProxyRequest;
use App\Proxy;
use App\Indicator;


class ProxyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proxies = Proxy::userLogin()->get();
        return view('admin.proxy.index', compact('proxies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indicators = Indicator::userLogin()->get();
        return view('admin.proxy.form', compact('indicators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProxyRequest $request)
    {
        try {
            $save = Proxy::create([
                // 'indicator_id' => $request->indicator_id,
                'name' => $request->name,
                // 'bobot' => $request->bobot,
                'user_id' => user_login()->id,
            ]);    

            return redirect(route('proxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indicators = Indicator::userLogin()->get();
        $proxy = Proxy::findOrFail($id);
        return view('admin.proxy.form', compact('proxy','indicators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProxyRequest $request, $id)
    {
        try {
            $proxy = Proxy::findOrFail($id);
            // $proxy->indicator_id = $request->indicator_id;    
            $proxy->name = $request->name;    
            // $proxy->bobot = $request->bobot; 
            $proxy->user_id = user_login()->id;
            $proxy->save();
            
            return redirect(route('proxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Proxy::destroy($id);

            return redirect(route('proxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
