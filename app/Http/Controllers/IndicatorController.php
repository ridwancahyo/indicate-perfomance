<?php

namespace App\Http\Controllers;

use App\Http\Requests\Indicator\IndicatorRequest;
use App\Http\Requests\Indicator\AddProxyToIndicator;
use App\Indicator;
use App\Position;
use App\Proxy;

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicators = Indicator::userLogin()->get();
        return view('admin.indicator.index', compact('indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $positions = Position::userLogin()->get();
        return view('admin.indicator.form', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IndicatorRequest $request)
    {
        try {
            $indicator = Indicator::create([
                'name' => $request->name,
                // 'bobot' => $request->bobot,
                // 'position_id' => $request->position_id,
                'description' => $request->description,
                'user_id' => user_login()->id,
            ]);

            return redirect(route('indicator.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indicator = Indicator::findOrFail($id);
        return view('admin.indicator.form', compact('indicator','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IndicatorRequest $request, $id)
    {
        try {
            $indicator = Indicator::findOrFail($id);
            
            $indicator->name = $request->name;
            // $indicator->bobot = $request->bobot;
            // $indicator->position_id = $request->position_id;
            $indicator->description = $request->description;
            $indicator->save();

            return redirect(route('indicator.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $indicator = Indicator::destroy($id);

            return redirect(route('indicator.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function GetProxy($indicator_id)
    {
        try {
            $proxies = Proxy::where('indicator_id',$indicator_id)->get();
            $otherProxies = Proxy::whereNull('indicator_id')->get();
            $indicator = Indicator::with('position')->where('id',$indicator_id)->first();

            if (empty($indicator->position->name)) {
                return "indikator belum di set ke jabatan";
            }

            return view('admin.indicator.proxy.index', compact('proxies','otherProxies','indicator'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function AddProxy(AddProxyToIndicator $request)
    {
        try {
            $proxy = Proxy::findOrFail($request->proxy_id);
            $proxy->indicator_id = $request->indicator_id;
            $proxy->bobot = $request->bobot;
            $proxy->save();
            
            return back();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
