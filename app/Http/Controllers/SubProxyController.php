<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubProxy\SubProxyRequest;
use App\SubProxy;
use App\Proxy;


class SubProxyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subproxies = SubProxy::userLogin()->get();
        return view('admin.subproxy.index', compact('subproxies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proxies = Proxy::userLogin()->get();
        return view('admin.subproxy.form', compact('proxies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubProxyRequest $request)
    {
        try {
            $save = SubProxy::create([
                // 'proxy_id' => $request->proxy_id,
                'name' => $request->name,
                'code' => $request->code,
                'user_id' => user_login()->id,
            ]);

            return redirect(route('subproxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subproxy = SubProxy::findOrFail($id);
        $proxies = Proxy::userLogin()->get();
        return view('admin.subproxy.form', compact('subproxy','proxies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubProxyRequest $request, $id)
    {
        try {
            $subproxy = SubProxy::findOrFail($id);
            // $subproxy->proxy_id = $request->proxy_id;
            $subproxy->name = $request->name;
            $subproxy->code = $request->code;
            $subproxy->user_id = user_login()->id;
            $subproxy->save();

            return redirect(route('subproxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            SubProxy::destroy($id);

            return redirect(route('subproxy.index'));
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
