<?php

namespace App\Http\Controllers;

use App\Satuan;
use App\Http\Requests\Satuan\SatuanRequest;


class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $satuans = Satuan::userLogin()->statusActived()->get();
        return view('admin.satuan.index', compact('satuans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.satuan.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SatuanRequest $request)
    {
        try {
            $position = Satuan::create(['name' => $request->name, 'user_id' => user_login()->id, 'status' => 1]);
            
            createSession('success','Data berhasil disimpan ...');
            return redirect(route('satuan.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $satuan = Satuan::findOrFail($id);
        return view('admin.satuan.form', compact('satuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SatuanRequest $request, $id)
    {
        try {
            $satuan = Satuan::findOrFail($id);
            $satuan->name = $request->name;
            $satuan->user_id = user_login()->id;
            $satuan->save();

            createSession('success','Data berhasil disimpan ...');
            return redirect(route('satuan.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $satuan = Satuan::findOrFail($id);
            $satuan->status = 0;
            $satuan->save();

            createSession('success','Data berhasil dihapus ...');
            return redirect(route('satuan.index'));
        } catch (\Throwable $th) {
            createSession('danger',$th->getMessage());
            return back();
        }
    }
}
