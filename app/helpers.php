<?php

function user_login()
{
    return Auth::user();
}

function createSession($alert, $message)
{
    session()->flash('alert',$alert);
    session()->flash('alert-message',$message);
    return true;
}

function getListProxyId($indicators)
{
    $list_proxy_id = [];
    foreach ($indicators as $indicator) {
        $list_proxy_id[$indicator->id] = $indicator->proxies->pluck('id');
    }

    $list_lagi = [];
    foreach ($list_proxy_id as $key => $value) {
        foreach ($value as $key => $value2) {
            $list_lagi[] = $value2;
        }
    }

    return $list_lagi;
}