<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'kode','nama','jenis_kelamin','email','no_telephone','position_id','user_id'
    ];

    public function position()
    {
        return $this->hasOne('App\Position', 'id','position_id');
    }

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }
}
