<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RumusSubProxy extends Model
{
    protected $fillable = ['rumus', 'proxy_id','user_id'];
    
    public function subproxy()
    {
        return $this->belongsTo('App\SubProxy', 'proxy_id', 'proxy_id');
    }

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }
}
