<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $fillable = [
        'name','user_id','status'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function scopeStatusActived($query)
    {
        return $query->where('status',1);
    }
}
