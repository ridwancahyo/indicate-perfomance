<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreIndicator extends Model
{
    protected $fillable = [
        'score', 'indicator_id', 'employee_id', 'user_id', 'recap'
    ];

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function scopeNoRecap($query)
    {
        return $query->where('recap', 0);
    }

    public function indicator()
    {
        return $this->hasOne('App\Indicator', 'id', 'indicator_id');
    }
}
