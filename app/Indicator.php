<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $fillable = [
        'name','bobot','user_id','position_id','description','target','satuan_id'
    ];

    public function job()
    {
        return $this->belongsTo('App\Job', 'indicator_id', 'id');
    }

    public function scopeUserLogin($query)
    {
        return $query->where('user_id', user_login()->id);
    }

    public function position()
    {
        return $this->hasOne('App\Position', 'id', 'position_id');
    }

    public function proxies()
    {
        return $this->hasMany('App\Proxy', 'indicator_id', 'id');
    }

    public function satuan()
    {
        return $this->hasOne('App\Satuan', 'id', 'satuan_id');
    }
}
